const IP_API = 'http://localhost';
const PORT_API = '5803';
const TAG_API = 'api';

export const environment = {
  production: false,
  HOST_API: IP_API + ':' + PORT_API + '/' + TAG_API + '/'
};
