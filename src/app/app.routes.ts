import { RouterModule, Routes       } from '@angular/router';
import { SellerEditComponent        } from './components/seller/edit/edit.component';
import { SellerDeleteComponent      } from './components/seller/delete/delete.component';
import { SellerRegisterComponent    } from './components/seller/register/register.component';
import { SellerListComponent        } from './components/seller/list/list.component';
import { SaleEditComponent          } from './components/sale/edit/edit.component';
import { SaleDeleteComponent        } from './components/sale/delete/delete.component';
import { SaleRegisterComponent      } from './components/sale/register/register.component';
import { SaleListComponent          } from './components/sale/list/list.component';
import { ProductEditComponent       } from './components/product/edit/edit.component';
import { ProductDeleteComponent     } from './components/product/delete/delete.component';
import { ProductRegisterComponent   } from './components/product/register/register.component';
import { ProductListComponent       } from './components/product/list/list.component';
import { OrderSaleEditComponent     } from './components/ordersale/edit/edit.component';
import { OrderSaleDeleteComponent   } from './components/ordersale/delete/delete.component';
import { OrderSaleRegisterComponent } from './components/ordersale/register/register.component';
import { OrderSaleListComponent     } from './components/ordersale/list/list.component';
import { ReportComponent            } from './components/report/report.component';
import { HomeComponent              } from './components/home/home.component';

const appRoutes: Routes = [
    { path: '',                     component: HomeComponent },
    { path: 'home',                 component: HomeComponent },
    { path: 'product',              component: ProductListComponent },
    { path: 'product/register',     component: ProductRegisterComponent },
    { path: 'product/edit/:id',     component: ProductEditComponent },
    { path: 'product/delete/:id',   component: ProductDeleteComponent },
    { path: 'sale',                 component: SaleListComponent },
    { path: 'sale/register',        component: SaleRegisterComponent },
    { path: 'sale/edit/:id',        component: SaleEditComponent },
    { path: 'sale/delete/:id',      component: SaleDeleteComponent },
    { path: 'seller',               component: SellerListComponent },
    { path: 'seller/register',      component: SellerRegisterComponent },
    { path: 'seller/edit/:id',      component: SellerEditComponent },
    { path: 'seller/delete/:id',    component: SellerDeleteComponent },        
    { path: 'sale/report',          component: ReportComponent },
    { path: 'ordersale',            component: OrderSaleListComponent },
    { path: 'ordersale/register',   component: OrderSaleRegisterComponent },
    { path: 'ordersale/edit/:id',   component: OrderSaleEditComponent },
    { path: 'ordersale/delete/:id', component: OrderSaleDeleteComponent },    
    { path: '**',                   component: HomeComponent }
];

export const routing = RouterModule.forRoot(appRoutes);