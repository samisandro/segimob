import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing } from './app.routes';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { AppComponent               } from './app.component';
import { SellerEditComponent        } from './components/seller/edit/edit.component';
import { SellerDeleteComponent      } from './components/seller/delete/delete.component';
import { SellerRegisterComponent    } from './components/seller/register/register.component';
import { SellerListComponent        } from './components/seller/list/list.component';
import { SaleEditComponent          } from './components/sale/edit/edit.component';
import { SaleDeleteComponent        } from './components/sale/delete/delete.component';
import { SaleRegisterComponent      } from './components/sale/register/register.component';
import { SaleListComponent          } from './components/sale/list/list.component';
import { ProductEditComponent       } from './components/product/edit/edit.component';
import { ProductDeleteComponent     } from './components/product/delete/delete.component';
import { ProductRegisterComponent   } from './components/product/register/register.component';
import { ProductListComponent       } from './components/product/list/list.component';
import { OrderSaleEditComponent     } from './components/ordersale/edit/edit.component';
import { OrderSaleDeleteComponent   } from './components/ordersale/delete/delete.component';
import { OrderSaleRegisterComponent } from './components/ordersale/register/register.component';
import { OrderSaleListComponent     } from './components/ordersale/list/list.component';
import { ReportComponent            } from './components/report/report.component';
import { HomeComponent              } from './components/home/home.component';
import { GenderPipe                 } from './util/pipe/gender.pipe';
import { NgbModule                  } from '@ng-bootstrap/ng-bootstrap';
import { HttpService                } from './services/http.service';
import { HttpModule                 } from '@angular/http';
import { OrderSaleService           } from './services/ordersale.service';
import { SaleService                } from './services/sale.service';
import { SellerService              } from './services/seller.service';
import { ProductService             } from './services/product.service';

@NgModule({
  declarations: [
    AppComponent              ,
    SellerEditComponent       , 
    SellerDeleteComponent     , 
    SellerRegisterComponent   , 
    SellerListComponent       , 
    SaleEditComponent         , 
    SaleDeleteComponent       , 
    SaleRegisterComponent     , 
    SaleListComponent         , 
    ProductEditComponent      , 
    ProductDeleteComponent    , 
    ProductRegisterComponent  , 
    ProductListComponent      , 
    OrderSaleEditComponent    , 
    OrderSaleDeleteComponent  , 
    OrderSaleRegisterComponent, 
    OrderSaleListComponent    , 
    ReportComponent           , 
    HomeComponent             ,
    GenderPipe
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: 
  [
    HttpService,
    OrderSaleService,
    SaleService,     
    SellerService,   
    ProductService,
    FormBuilder
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
