export class Sale {
    public Id: number;  
    public DateSale: string;
    public CommissionValue: string;
    public TotalValue: any;
    public IdSeller: number;
    public NameSeller: string;
}
