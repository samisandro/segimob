export class Seller {
    public Id: number;
    public Name: string;
    public DocumentId: string;
    public Salary: string;   
    public BirthDate: string;
    public Gender: string;
    public Address: string;
}
