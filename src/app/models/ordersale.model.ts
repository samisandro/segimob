export class OrderSale {
    public Id: number;
    public IdSale: number;
    public IdProduct: number;        
    public Quantity: string;
    public NameProduct: string;
}
