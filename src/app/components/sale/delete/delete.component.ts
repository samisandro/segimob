import { Component, OnInit } from '@angular/core';
import { Sale } from '../../../models/sale.model';
import { ActivatedRoute, Router } from '@angular/router';
import { SaleService } from '../../../services/sale.service';
import { AppUtils } from '../../../util/app.util';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class SaleDeleteComponent implements OnInit {

  public sale: Sale;
  id: number;
  error: boolean;
  errorMsg: string;
  success: boolean;
  successMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: SaleService
  ) {
    
    this.successMsg = "Cadastro deletado com sucesso.";
    this.errorMsg = "Erro remover, tente novamente!";
    this.id = route.snapshot.params['id'];
    this.sale = new Sale();
    this.error = false;
    this.success = false;
   }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.sale = res;
      this.sale.DateSale = AppUtils.getBRDateLocaleFormat(this.sale.DateSale);
    });
  }
  
  remove() {
    this.api.delete(this.id).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['person']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
  }
