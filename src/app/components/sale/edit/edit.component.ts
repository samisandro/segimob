import { Component, OnInit } from '@angular/core';
import { SaleService } from '../../../services/sale.service';
import { Sale } from '../../../models/sale.model';
import { Seller } from '../../../models/seller.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AppUtils } from '../../../util/app.util';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class SaleEditComponent implements OnInit {

  public sale: Sale;
  id: number;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: SaleService
    
  ) {
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro atualizado.";
    this.errorMsg = "Erro ao atualizar, tente novamente!";
    this.id = route.snapshot.params['id'];
    this.sale = new Sale();
  }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.sale = res;
      this.sale.DateSale = AppUtils.getDateLocaleFormat(this.sale.DateSale);
    });
  }
  update() {
    this.api.update(this.id, this.sale).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['person']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  validateForm() {
    if (
      !this.sale.NameSeller
      || !this.sale.TotalValue
      || !this.sale.DateSale
      || !this.sale.CommissionValue) {
      this.showWarning(this.formErrorMsg);
    } else {
      this.update();
    }

  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showWarning(msg: string) {
    this.formErrorMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
}
