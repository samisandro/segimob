import { Component, OnInit } from '@angular/core';
import { SaleService } from '../../../services/sale.service';



@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class SaleListComponent implements OnInit {
  
  public listSale: any;
  public saveResult: any;
  public NameSeller: any;
  public showLoading: boolean;

  constructor(private api: SaleService) { }

  ngOnInit(self = this) {
    this.api.getAll().subscribe(res => {
      this.listSale = res;
    });

  }  
}

