import { Component, OnInit } from '@angular/core';
import { Sale } from '../../../models/sale.model';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { SaleService } from '../../../services/sale.service';
import { Router } from '@angular/router';
import {MatOption, MatSelect,MatFormField} from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class SaleRegisterComponent implements OnInit {

  public sale: Sale;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;
  sellers: any = [];
  value: any;


  constructor(
    private api: SaleService,
    private router: Router
  ) { 
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro realizado com sucesso.";
    this.errorMsg = "Erro ao cadastrar, tente novamente!";
    this.sale = new Sale();
  }

  ngOnInit() {   
     }

  calcCommission(){    
    this.value = this.sale.TotalValue * 0.05;
     }

  save() {
    this.api.create(this.sale).subscribe(res => {
      if (res.status === 200) {
        this.cleanForm();
        this.showSuccess(this.successMsg);
        this.router.navigate(['sale']);
      } else {
        this.showError(this.errorMsg);
      }
    })
  }

  cleanForm() {
    this.sale.NameSeller = "";
    this.sale.DateSale = "";
    this.sale.CommissionValue = "";
    this.sale.TotalValue = "";  
  }

  validateForm() {
    if (    
      !this.sale.NameSeller
      || !this.sale.DateSale
      || !this.sale.CommissionValue
      || !this.sale.TotalValue) {
      this.showWarning(this.formErrorMsg);
    } else {
      this.save();
    }
  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showWarning(msg: string) {
    this.formErrorMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
}
