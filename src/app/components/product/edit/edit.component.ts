import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/product.service';
import { Product } from '../../../models/product.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class ProductEditComponent implements OnInit {

  public product: Product;
  id: number;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ProductService
    
  ) {
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro atualizado.";
    this.errorMsg = "Erro ao atualizar, tente novamente!";
    this.id = route.snapshot.params['id'];
    this.product = new Product();
  }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.product = res;     
    });
  }
  update() {
    this.api.update(this.id, this.product).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['product']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  validateForm() {
    if (!this.product.Name || !this.product.Value) {      
      this.showWarning(this.formErrorMsg);
    } else {
      debugger;
      this.product.Value = this.product.Value.replace(",",".");
      this.update();
    }

  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showWarning(msg: string) {
    this.formErrorMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
}
