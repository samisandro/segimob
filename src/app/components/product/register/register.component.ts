import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/product.model';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ProductService } from '../../../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class ProductRegisterComponent implements OnInit {

  public product: Product;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;

  constructor(
    private api: ProductService,
    private router: Router
  ) { 
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro realizado com sucesso.";
    this.errorMsg = "Erro ao cadastrar, tente novamente!";
    this.product = new Product();
  }

  ngOnInit() {
  }

  save() {
    this.api.create(this.product).subscribe(res => {
      if (res.status === 200) {
        this.cleanForm();
        this.showSuccess(this.successMsg);
        this.router.navigate(['product']);
      } else {
        this.showError(this.errorMsg);
      }
    })
  }

  cleanForm() {
    this.product.Name = "";
    this.product.Value = "";      
  }

  validateForm() {        
    if (!this.product.Name || !this.product.Value) {      
      this.showWarning(this.formErrorMsg);
    } else {     
      this.save();
    }
  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showWarning(msg: string) {
    this.formErrorMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
}
