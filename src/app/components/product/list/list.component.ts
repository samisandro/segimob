import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ProductListComponent implements OnInit {
  public listProduct: any;
  public saveResult: any;

  public showLoading: boolean;
  constructor(private api: ProductService) { }

  ngOnInit(self = this) {
    this.api.getAll().subscribe(res => {
      this.listProduct = res;
    });
  }
}
