import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/product.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../../services/product.service';


@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class ProductDeleteComponent implements OnInit {

  public product: Product;
  id: number;
  error: boolean;
  errorMsg: string;
  success: boolean;
  successMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ProductService
  ) {
    
    this.successMsg = "Cadastro deletado com sucesso.";
    this.errorMsg = "Erro remover, tente novamente!";
    this.id = route.snapshot.params['id'];
    this.product = new Product();
    this.error = false;
    this.success = false;
   }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.product = res;
    });
  }
  
  remove() {
    this.api.delete(this.id).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['product']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  showSuccess(msg: string) {
    this.successMsg;
  }
  
  showError(msg: string) {
    this.errorMsg;
  }
}
