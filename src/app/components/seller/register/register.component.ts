import { Component, OnInit } from '@angular/core';
import { Seller } from '../../../models/seller.model';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { SellerService } from '../../../services/seller.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class SellerRegisterComponent implements OnInit {

  public seller: Seller;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;

  constructor(
    private api: SellerService,
    private router: Router
  ) { 
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro realizado com sucesso.";
    this.errorMsg = "Erro ao cadastrar, tente novamente!";
    this.seller = new Seller();
  }

  ngOnInit() {
  }

  save() {
    this.api.create(this.seller).subscribe(res => {
      if (res.status === 200) {
        this.cleanForm();
        this.showSuccess(this.successMsg);
        this.router.navigate(['seller']);
      } else {
        this.showError(this.errorMsg);
      }
    })
  }

  cleanForm() {
    this.seller.Name = "";
    this.seller.DocumentId = "";
    this.seller.BirthDate = "";
    this.seller.Gender = "";
    this.seller.Address = "";    
    this.seller.Salary = "";    
  }

  validateForm() {
    if (    
      !this.seller.Name
      || !this.seller.DocumentId
      || !this.seller.BirthDate
      || !this.seller.Gender
      || !this.seller.Salary
      || !this.seller.Address) {
      this.showWarning(this.formErrorMsg);
    } else {
      this.save();
    }
  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showWarning(msg: string) {
    this.formErrorMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
}
