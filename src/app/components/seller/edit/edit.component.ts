import { Component, OnInit } from '@angular/core';
import { SellerService } from '../../../services/seller.service';
import { Seller } from '../../../models/seller.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AppUtils } from '../../../util/app.util';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class SellerEditComponent implements OnInit {

  public seller: Seller;
  id: number;
  errorMsg: string;
  successMsg: string;
  formErrorMsg: string;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: SellerService
    
  ) {
    this.formErrorMsg = "Preencha os campos obrigatórios.";
    this.successMsg = "Cadastro atualizado.";
    this.errorMsg = "Erro ao atualizar, tente novamente!";
    this.id = route.snapshot.params['id'];
    this.seller = new Seller();
  }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.seller = res;
      this.seller.BirthDate = AppUtils.getDateLocaleFormat(this.seller.BirthDate);
    });
  }
  update() {
    this.api.update(this.id, this.seller).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['seller']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }

  validateForm() {
    debugger;
    if (
      !this.seller.Name
      || !this.seller.DocumentId
      || !this.seller.BirthDate
      || !this.seller.Gender
      || !this.seller.Salary
      || !this.seller.Address) {
      this.showWarning(this.formErrorMsg);
    } else {
      this.seller.Salary = this.seller.Salary.replace(",",".");
      this.update();
    }

  }

  showSuccess(msg: string) {
    this.successMsg;
  }

  showWarning(msg: string) {
    this.formErrorMsg;
  }

  showError(msg: string) {
    this.errorMsg;
  }
}
