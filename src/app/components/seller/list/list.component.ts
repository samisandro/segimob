import { Component, OnInit } from '@angular/core';
import { SellerService } from '../../../services/seller.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class SellerListComponent implements OnInit {
  public listSeller: any;
  public saveResult: any;

  public showLoading: boolean;

  constructor(private api: SellerService) { }

  ngOnInit(self = this) {
    this.api.getAll().subscribe(res => {
      this.listSeller = res;
    });
  }
}
