import { Component, OnInit } from '@angular/core';
import { Seller } from '../../../models/seller.model';
import { ActivatedRoute, Router } from '@angular/router';
import { SellerService } from '../../../services/seller.service';
import { AppUtils } from '../../../util/app.util';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class SellerDeleteComponent implements OnInit {

  public seller: Seller;
  id: number;
  error: boolean;
  errorMsg: string;
  success: boolean;
  successMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: SellerService
  ) {
    
    this.successMsg = "Cadastro deletado com sucesso.";
    this.errorMsg = "Erro remover, tente novamente!";
    this.id = route.snapshot.params['id'];
    this.seller = new Seller();
    this.error = false;
    this.success = false;
   }

  ngOnInit() {
    this.api.get(this.id).subscribe(res => {
      this.seller = res;
      this.seller.BirthDate = AppUtils.getBRDateLocaleFormat(this.seller.BirthDate);
    });
  }
  
  remove() {
    this.api.delete(this.id).subscribe(res => {
      if (res.status === 200) {
        this.showSuccess(this.successMsg);
        this.router.navigate(['seller']);
      } else {
        this.showError(this.errorMsg);
      }
    });
  }
  showSuccess(msg: string) {
    this.successMsg;
  }
  
  showError(msg: string) {
    this.errorMsg;
  }
}
