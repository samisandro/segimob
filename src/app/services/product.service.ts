
import { HttpService } from './http.service';
import 'rxjs/add/operator/map';
import { Product } from '../models/product.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ProductService {
    private api: string;
    constructor(private http: Http, private httpService: HttpService) {
        this.api = httpService.getSegImobApi() + 'product/';
    }

    get(id: number) {
        if (id && id > 0) {
            return this.http.get(this.api + id)
                .map(res => {
                    return res.json();
                });
        }
    }

    getAll() {
        return this.http.get(this.api)
            .map(res => {
                return res.json();
            });
    }   

    create(product: Product) {
        const header = this.httpService.getHeaders();
        return this.http.post(this.api, product, header)
            .map(res => {
                return res;
            });
    }

    delete(id: number) {
        return this.http.delete(this.api + id)
            .map(res => {
                return res;
            });
    }

    update(id: number, product: Product) {
        if (id && id > 0) {
            product.Id = id;
            const header = this.httpService.getHeaders();
            return this.http.put(this.api + id, product, header)
                .map(res => {
                    return res;
                });
        }
    }

   
}
