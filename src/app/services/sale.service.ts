
import { HttpService } from './http.service';
import 'rxjs/add/operator/map';
import { Sale } from '../models/sale.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SaleService {
    private api: string;
    constructor(private http: Http, private httpService: HttpService) {
        this.api = httpService.getSegImobApi() + 'sale/';
    }

    get(id: number) {
        if (id && id > 0) {
            return this.http.get(this.api + id)
                .map(res => {
                    return res.json();
                });
        }
    }

    getAll() {
        return this.http.get(this.api)
            .map(res => {
                return res.json();
            });
    }  
    
    getDTO() {
        debugger;
        return this.http.get(this.api)
            .map(res => {
                return res.json();
            });
    }  

    create(sale: Sale) {
        const header = this.httpService.getHeaders();
        return this.http.post(this.api, sale, header)
            .map(res => {
                return res;
            });
    }

    delete(id: number) {
        return this.http.delete(this.api + id)
            .map(res => {
                return res;
            });
    }

    update(id: number, sale: Sale) {
        if (id && id > 0) {
            sale.Id = id;
            const header = this.httpService.getHeaders();
            return this.http.put(this.api + id, sale, header)
                .map(res => {
                    return res;
                });
        }
    }

   
}
