
import { HttpService } from './http.service';
import 'rxjs/add/operator/map';
import { Seller } from '../models/seller.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SellerService {
    private api: string;
    constructor(private http: Http, private httpService: HttpService) {
        this.api = httpService.getSegImobApi() + 'seller/';
    }

    get(id: number) {
        if (id && id > 0) {
            return this.http.get(this.api + id)
                .map(res => {
                    return res.json();
                });
        }
    }

    getAll() {
        return this.http.get(this.api)
            .map(res => {
                return res.json();
            });
    }   

    create(seller: Seller) {
        const header = this.httpService.getHeaders();
        return this.http.post(this.api, seller, header)
            .map(res => {
                return res;
            });
    }

    delete(id: number) {
        return this.http.delete(this.api + id)
            .map(res => {
                return res;
            });
    }

    update(id: number, seller: Seller) {
        if (id && id > 0) {
            seller.Id = id;
            const header = this.httpService.getHeaders();
            return this.http.put(this.api + id, seller, header)
                .map(res => {
                    return res;
                });
        }
    }

   
}
