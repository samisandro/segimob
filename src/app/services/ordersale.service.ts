
import { HttpService } from './http.service';
import 'rxjs/add/operator/map';
import { OrderSale } from '../models/ordersale.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class OrderSaleService {
    private api: string;
    constructor(private http: Http, private httpService: HttpService) {
        this.api = httpService.getSegImobApi() + 'ordersale/';
    }

    get(id: number) {
        if (id && id > 0) {
            return this.http.get(this.api + id)
                .map(res => {
                    return res.json();
                });
        }
    }

    getAll() {
        return this.http.get(this.api)
            .map(res => {
                return res.json();
            });
    }   

    create(ordersale: OrderSale) {
        const header = this.httpService.getHeaders();
        return this.http.post(this.api, ordersale, header)
            .map(res => {
                return res;
            });
    }

    delete(id: number) {
        return this.http.delete(this.api + id)
            .map(res => {
                return res;
            });
    }

    update(id: number, ordersale: OrderSale) {
        if (id && id > 0) {
            ordersale.Id = id;
            const header = this.httpService.getHeaders();
            return this.http.put(this.api + id, ordersale, header)
                .map(res => {
                    return res;
                });
        }
    }

   
}
